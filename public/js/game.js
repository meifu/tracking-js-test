/** @module for tracking js */
var videoCamera = new tracking.VideoCamera().hide().render()/*.renderVideoCanvas()*/;
var ctx = videoCamera.canvas.context;

/** record the number that left/right eye blinks */
var leftBlink = 0, rightBlink = 0;

videoCamera.track({
    type: 'human',
    data: 'eye',
    onFound: function(track) {
        //console.log(track);
        
        for (var i = 0, len = track.length; i < len; i++) {
            var rect = track[i];
            
            //console.log('x:'+ track[i].x + 'y:' + track[i].y);

            /** xPos, yPos are eye position */
            var xPos = track[i].x;
            var yPos = track[i].y;

            if ( xPos > 300 ) { /** 300 is half of the video canvas */
                console.log('left');
                ctx.strokeStyle = "rgb(0,255,0)";
                ctx.strokeRect(rect.x, rect.y, rect.size, rect.size);
                leftBlink = leftBlink + 1;

            } else if ( xPos < 300 ){
                //console.log('right');
                // ctx.strokeStyle = "rgb(255,0,0)";
                // ctx.strokeRect(rect.x, rect.y, rect.size, rect.size);
                // rightBlink = rightBlink + 1;
            }
                 
            //console.log('left:'+ leftBlink + 'right:' + rightBlink);
            document.getElementById('leftEyeCnt').innerHTML = leftBlink;
            if ( leftBlink > 10 ) {
                window.location.href="/pageControl.html";
            }
            
        } //end for loop
        
    }, 
    onNotFound: function(track) {
        console.log('notfound');
    }
    
});