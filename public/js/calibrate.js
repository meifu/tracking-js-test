
/** @module for svg.js to draw */

/** some basic variables */
var browserWidth = window.document.width;
var browserHeight = window.document.height;
var draw = SVG('calibrateCanvas').size(browserWidth, browserHeight);
var horizontalPath = 'M0 ' + browserHeight/2 + ' H ' + browserWidth ;
var verticalPath = 'M' + browserWidth/2 + ',0' + 'V ' + browserHeight ;
var hLine = draw.path(horizontalPath, true).stroke({ width: 1, color: '#000' });
var vLine = draw.path(verticalPath, true).stroke({ width: 1, color: '#000' });
// var test = draw.path('M0,421H600', true).stroke({ width: 1, color: '#000' });


/** @module for tracking js */
var videoCamera = new tracking.VideoCamera().hide().render().renderVideoCanvas();
var ctx = videoCamera.canvas.context;

/** record the number that left/right eye blinks */
var leftBlink = 0, rightBlink = 0;

videoCamera.track({
    type: 'human',
    data: 'eye',
    onFound: function(track) {
        //console.log(track);
        
        for (var i = 0, len = track.length; i < len; i++) {
            var rect = track[i];
            // ctx.strokeStyle = "rgb(0,255,0)";
            console.log('x:'+ track[i].x + 'y:' + track[i].y);

            /** xPos, yPos are eye position */
            var xPos = track[i].x;
            var yPos = track[i].y;

            if ( xPos > 300 ) { /** 300 is half of the video canvas */
                console.log('left');
                ctx.strokeStyle = "rgb(0,255,0)";
                ctx.strokeRect(rect.x, rect.y, rect.size, rect.size);
                leftBlink = leftBlink + 1;

            } else if ( xPos < 300 ){
                console.log('right');
                ctx.strokeStyle = "rgb(255,0,0)";
                ctx.strokeRect(rect.x, rect.y, rect.size, rect.size);
                rightBlink = rightBlink + 1;
            }
                 
            console.log('left:'+ leftBlink + 'right:' + rightBlink);
            if ( leftBlink > 2 && rightBlink > 2 ) {
                setTimeout(function(){
                    window.location.href = 'game.html';
                }, 600);
                
            }
        } //end for loop


        
    }
});


