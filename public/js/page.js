/** get the page number */
var pageNum = document.getElementById('pageTitle').getAttribute('data-page');
console.log(pageNum);

/** initial eye position */
var leftEyeX, leftEyeY, rightEyeX, rightEyeY, averageX, averageY;
// leftEyeX = leftEyeY = rightEyeX = rightEyeY = 0;

/** @module tracking js */
var videoCamera = new tracking.VideoCamera().hide()/*.render().renderVideoCanvas()*/;
var ctx = videoCamera.canvas.context;


videoCamera.track({
    type: 'human',
    data: 'eye',
    onFound: function(track) {

    	/** sumX and sumY are used to calculate the average x and y positions of user's eye */
    	var sumX = 0, sumY = 0;
    	for (var i = 0, len = track.length; i < len; i++) {
    		var rect = track[i];
    		ctx.strokeStyle = "rgb(0,255,0)";
    		ctx.strokeRect(rect.x, rect.y, rect.size, rect.size);

    		var sumX = sumX + track[i].x;
    		var sumY = sumY + track[i].y;
    	}
    	/** from this capture, calculate the average x, y positions of user's eye */ 
    	averageX = sumX/track.length;
    	averageY = sumY/track.length;
    	console.log('aveX: ' + averageX + '. aveY: ' + averageY);

    	if (averageX < 200) {
    		$('#goRight').click();
    	} else if (averageX > 400) {
            $('#goLeft').click();
        } else if (averageY < 100) {
            $('#goUp').click();
        } else if (averageY > 300) {
            $('#goDown').click();
        }
    },
    onNotFound: function(track) {
    	console.log('no eye');
    }

});