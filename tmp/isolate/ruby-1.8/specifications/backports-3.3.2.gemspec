# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = "backports"
  s.version = "3.3.2"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["Marc-Andr\303\251 Lafortune"]
  s.date = "2013-06-13"
  s.description = "Essential backports that enable many of the nice features of Ruby 1.8.7 up to 2.0.0 for earlier versions."
  s.email = ["github@marc-andre.ca"]
  s.homepage = "http://github.com/marcandre/backports"
  s.require_paths = ["lib"]
  s.rubygems_version = "2.0.3"
  s.summary = "Backports of Ruby features for older Ruby."
end
