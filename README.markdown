# Project - Test Tracking JS #

## Main Demo File: pageControl.html ##
>
>* 用眼睛位置控制DIV移動的方向
>* 有 上，中，下，右，左 五個DIV
>* tracking.js
>* tracker/human/human.min.js
>* tracker/human/data/eye.min.js
>

## Below are some problems: ##
>
>1. 抓眼睛很不穩定 會抓不到或抓錯
>2. camera sync很lag
>3. onNotFound根本沒作用？

### other plugins include: ###
>
>1. ElementTransitions.js
> (http://dan-silver.github.io/ElementTransitions.js/)
>2. jsdoc
>
	<p>test code block</p>


## Other Demo File 1: index.html ##
>
> calibrate.js
> 畫十字讓使用者校正眼睛位置
> video canvas寬度是600
> 如果偵測到眼睛的X小於300-->左眼
> 如果偵測到眼睛的X大於300-->右眼
> 如果兩眼都被偵測到超過2次，跳到game.html
>

## Other Demo File 2: game.html ##
>
> game.js
> 偵測到左眼>10次，跳到pageControl.html
>

## Other Demo File 3: colorTest.html ##
>
> color.js
> 測試有偵測到cyan
>

## Other Demo File 4: css_filter.html ##
>
> color.js
> 用css把畫面上所有顏色反轉
>

## Other Demo File 5: frontalface.html ##
>
> tracker/human/data/frontal_face.js
> 測試偵測到人臉
>




